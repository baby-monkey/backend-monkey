package com.babymonkey.musicmonkey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusicmonkeyApplication {

    public static void main(String[] args) {
        SpringApplication.run(MusicmonkeyApplication.class, args);
    }

}
